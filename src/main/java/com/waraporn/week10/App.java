package com.waraporn.week10;

public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimetor());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimetor());

        Circle cir = new Circle(2);
        System.out.println(cir);
        System.out.printf("%s area: %.3f \n",cir.getName(),cir.calArea());
        System.out.printf("%s perimetor: %.3f \n",cir.getName(),cir.calPerimetor());

        Circle cir2 = new Circle(3);
        System.out.println(cir2);
        System.out.printf("%s area: %.3f \n",cir.getName(),cir2.calArea());
        System.out.printf("%s perimetor: %.3f \n",cir.getName(),cir2.calPerimetor());

        Triangle tri = new Triangle(2, 2, 2);
        System.out.println(tri);
        System.out.printf("%s area: %.3f \n",tri.getName(),tri.calArea());
        System.out.printf("%s perimetor: %.3f \n",tri.getName(),tri.calPerimetor());
    }
}
