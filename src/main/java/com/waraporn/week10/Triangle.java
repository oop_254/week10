package com.waraporn.week10;

public class Triangle extends Shape{
    private double A;
    private double B;
    private double C;
    public Triangle(double A, double B, double C) {
        super("triangle:");
        this.A = A;
        this.B = B;
        this.C = C;
    }
    @Override
    public double calArea() {
        return Math.sqrt(calPerimetor()*(calPerimetor()-A)*(calPerimetor()-B)*(calPerimetor()-C));
    }
    @Override
    public double calPerimetor() {
        return (this.A + this.B + this.C)/2;
    }
    @Override
    public String toString() {
        return this.getName() + "A:"+ this.A + " " + "B:" + this.B + " " + "C:" +this.C;
    }
}
